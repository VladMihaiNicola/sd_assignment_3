<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../layouts/taglib.jsp" %>

<table class="table table-bordered table-hover table-striped">
<thead>
	<tr>
		<th> User Name </th>
		<th> Password </th>
		<th> Id </th>
	</tr>
</thead>
<tbody>
	<c:forEach items="${users }" var="user">
		<tr>
			<td>${user.getUsername() } </td>
			<td>${user.getPassword() }	</td>
			<td>${user.getId() } </td>	
		</tr>
	</c:forEach>
</tbody>
</table>