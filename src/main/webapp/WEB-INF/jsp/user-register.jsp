<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <%@ include file="../layouts/taglib.jsp" %>
 
 <form:form commandName="user" cssClass="form-horizontal">
 		<div class="form-group">
 			<label for="username" class="col-sm-2 control-label">Username: </label>
 			<div class="col-sm-10">
 				<form:input cssClass="form-control" path="username" />
 			</div>
 		</div>
 		<div class="form-group">
 			<label for="password" class="col-sm-2 control-label">Password: </label>
 			<div class="col-sm-10">
 				<form:password cssClass="form-control" path="password" />
 			</div>
 		</div>
 		<div class="form-group">
 			<label for="password" class="col-sm-2 control-label">Confirm Password: </label>
 			<div class="col-sm-10">
 				<input type="password" class="form-control" name="confPass"/>
 			</div>
 		</div>
 		<div class="form-group">
 			<div class="col-sm-2">
 				<input type="submit" value="Save" class="btn btn-primary" />
 			</div>
 		</div>
 </form:form>