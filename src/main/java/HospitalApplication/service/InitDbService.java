package HospitalApplication.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HospitalApplication.entity.Role;
import HospitalApplication.entity.User;
import HospitalApplication.repository.ConsultationRepository;
import HospitalApplication.repository.PatientRepository;
import HospitalApplication.repository.RoleRepository;
import HospitalApplication.repository.UserRepository;

@Transactional
@Service
public class InitDbService {

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ConsultationRepository consultationRepository;
	
	@Autowired
	private PatientRepository patientRepository;
	
	@PostConstruct
	public void init()
	{
		Role roleAdmin = new Role();
		roleAdmin.setName("ROLE_ADMIN");
		roleRepository.save(roleAdmin);
		
		Role roleDoctor = new Role();
		roleDoctor.setName("ROLE_DOCTOR");
		roleRepository.save(roleDoctor);
		
		Role roleSecretary = new Role();
		roleSecretary.setName("ROLE_SECRETARY");
		roleRepository.save(roleSecretary);
		
		User userAdmin = new User();
		userAdmin.setUsername("admin");
		userAdmin.setPassword("admin");
		List<Role> rolesAdmin = new ArrayList<Role>();
		rolesAdmin.add(roleAdmin);
		userAdmin.setRoles(rolesAdmin);
		userRepository.save(userAdmin);
		
		User userSecretary = new User();
		userSecretary.setUsername("secretary");
		userSecretary.setPassword("secretary");
		List<Role> rolesSecretary = new ArrayList<Role>();
		rolesSecretary.add(roleSecretary);
		userSecretary.setRoles(rolesSecretary);
		userRepository.save(userSecretary);
		
		User userDoctor = new User();
		userDoctor.setUsername("doctor");
		userDoctor.setPassword("doctor");
		List<Role> rolesDoctor = new ArrayList<Role>();
		rolesDoctor.add(roleDoctor);
		userDoctor.setRoles(rolesDoctor);
		userRepository.save(userDoctor);
	}
}
