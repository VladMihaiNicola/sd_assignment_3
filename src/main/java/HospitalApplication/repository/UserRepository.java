package HospitalApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import HospitalApplication.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
