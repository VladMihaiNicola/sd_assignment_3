package HospitalApplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import HospitalApplication.entity.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer>{

}
